'use strict'

let matrizTablero;

let fichaAmarilla;
let fichaRoja;

let fichaAmarillaSelecc;
let fichaRojaSelecc;

var fichaActual;
var seleccionActual;

//Primero cargamos todos los elementos del DOM
window.addEventListener('load', function() {
    //nos creamos la matriz del tablero
    matrizTablero = crearMatriz();

    //Estados de las fichas. Estado Roja
    fichaAmarilla = `casillaAmarillaMarcada`;
    fichaRoja = `casillaRojaMarcada`;

    fichaAmarillaSelecc = `casillaAmarillaSeleccionada`;
    fichaRojaSelecc = `casillaRojaSeleccionada`;

    fichaActual = fichaRoja;
    seleccionActual = fichaRojaSelecc;


    // A partir de la matriz nos creamos la tabla en nuestro 'index.html'
    generarTablerodesdeMatriz(matrizTablero);

    //Bindeamos la tabla/tablero
    let tablero = document.querySelector('#tablero-juego');

    //TODO: Cambio de ficha

    //Creamos evento 'mouseover' y seleccionamos la columna
    tablero.addEventListener('mouseover', function(e) {
        if (e.target.tagName === 'TD') {
            //Obtenemos la columna y el número total de filas
            let columna = e.target.id.charAt(2);
            let numFilas = matrizTablero.length - 1;

            //Obtenemos las coordenadas y pintamos todas las casillas 
            for (let i = numFilas; i >= 0; i--) {
                let coord = i + '-' + columna;
                if (document.getElementById(`${coord}`).className !== seleccionActual && !existeFicha(coord)) {
                    document.getElementById(`${coord}`).classList.remove('casilla');
                    document.getElementById(`${coord}`).classList.add(seleccionActual);
                }
            }
        }
    });

    //Creamos evento 'mouseout' y seleccionamos la columna
    tablero.addEventListener('mouseout', function(e) {
        if (e.target.tagName === 'TD') {
            //Obtenemos la columna y el número total de filas
            let columna = e.target.id.charAt(2);
            let numFilas = matrizTablero.length - 1;

            //Obtenemos las coordenadas y pintamos todas las casillas 
            for (let i = numFilas; i >= 0; i--) {
                let coord = i + '-' + columna;
                if ((document.getElementById(`${coord}`).className === fichaAmarillaSelecc ||
                        document.getElementById(`${coord}`).className === fichaRojaSelecc) && !existeFicha(coord)) {
                    document.getElementById(`${coord}`).classList.remove(fichaAmarillaSelecc);
                    document.getElementById(`${coord}`).classList.remove(fichaRojaSelecc);
                    document.getElementById(`${coord}`).classList.add('casilla');
                }
            }

        }
    });

    //-----------------
    //Creamos evento 'click' y pintamos la columna seleccionada y la última fila
    tablero.addEventListener('click', function(e) {
        if (e.target.tagName === 'TD') {
            let columna = e.target.id.charAt(2);
            let numFilas = matrizTablero.length - 1;

            for (let i = numFilas; i >= 0; i--) {
                let coord = i + '-' + columna;

                if (document.getElementById(`${coord}`).className === seleccionActual) {
                    document.getElementById(`${coord}`).classList.remove(seleccionActual);
                    document.getElementById(`${coord}`).classList.add(fichaActual);
                    i = 0;
                }
            }
            //Establecemos/cambiamos el turno
            comprobarContinuacionJuego();
            turnoJugador(columna);

        }
    });

});


//------------------------------------
// Funciones
//------------------------------------

//Nos crea la matriz
function crearMatriz() {
    //Nos creamos la matriz
    let matriz = new Array(6);

    for (let i = 0; i < matriz.length; i++) {
        matriz[i] = new Array(8);
    }

    rellenarMatrizVacia(matriz);
    return matriz;
}


//Rellenamos la matriz con 'empty'
function rellenarMatrizVacia(matriz) {
    for (let i = 0; i < matriz.length; i++) {
        for (let j = 0; j < matriz[i].length; j++) {
            matriz[i][j] = i + `-` + j;
        }
    }
}

//Generamos la tabla en el html
function generarTablerodesdeMatriz(matriz) {
    let tablero = document.createElement('table');
    let tableBody = document.createElement('tbody');
    let idFila = 0;

    matriz.forEach(function(datosFila) {
        let fila = document.createElement('tr');
        fila.id = idFila++;

        datosFila.forEach(function(datosColum) {
            let columnaCasilla = document.createElement('td');
            //añadimos la clase 'casilla'
            columnaCasilla.classList.add('casilla');
            columnaCasilla.id = datosColum;
            //columnaCasilla.appendChild(document.createTextNode(datosColum));
            fila.appendChild(columnaCasilla);
        });

        tableBody.appendChild(fila);
    });
    tablero.appendChild(tableBody);

    let classesTablero = ['bg-primary', 'table'];
    tablero.classList.add(...classesTablero);
    tablero.id = `tablero-juego`;

    //añadimos los estilos de bootstrap y demás
    document.querySelector("#contenedor-principal").appendChild(tablero);
}


//Cambio de Jugador
function turnoJugador(columna) {
    //Guardamos el valor anterior
    let seleccionAnterior = seleccionActual;

    if (fichaActual === fichaRoja) {
        fichaActual = fichaAmarilla;
        seleccionActual = fichaAmarillaSelecc;

    } else {
        fichaActual = fichaRoja;
        seleccionActual = fichaRojaSelecc;
    }

    for (let i = matrizTablero.length - 1; i >= 0; i--) {
        let coord = i + '-' + columna;
        if (!existeFicha(coord)) {
            document.getElementById(`${coord}`).classList.remove(seleccionAnterior);
            document.getElementById(`${coord}`).classList.add(seleccionActual);
        }
    }
}

function existeFicha(coord) {
    return document.getElementById(`${coord}`).className === fichaRoja || document.getElementById(`${coord}`).className === fichaAmarilla;
}

//Comprobamos ganador
function comprobarContinuacionJuego() {
    const MAXfilas = matrizTablero.length - 1;
    const MAXcolumnas = matrizTablero[0].length - 1;
    var continuaJuego = false;

    //Recorremos la tabla en orden
    for (let i = 0; i <= MAXfilas && !continuaJuego; i++) {
        for (let j = 0; j <= MAXcolumnas && !continuaJuego; j++) {
            let coord = i + '-' + j;

            //Comprobamos si hay casillas libres
            if (document.getElementById(`${coord}`).className === 'casilla' || document.getElementById(`${coord}`).className === seleccionActual) {
                continuaJuego = true;
            }
            //console.log(continuaJuego);
        }
    }
    if (continuaJuego) {
        if (comprobarGanador(fichaActual)) {
            alert('Fin del juego');
        }
    } else {
        alert('Fin del juego');
    }
}





//Comprobación de si hay ganador
function comprobarGanador(tipoFicha) {
    const MAXfilas = matrizTablero.length - 1;
    const MAXcolumnas = matrizTablero[0].length - 1;
    var ganador = false;

    //Recorremos la tabal en orden:

    //Normal
    for (let fila = 0; fila <= MAXfilas && !ganador; fila++) {
        for (let columna = 0; columna <= MAXcolumnas && !ganador; columna++) {
            let coord = fila + '-' + columna;

            //Fila arriba
            if ((fila - 3) >= 0) {
                let coord1 = fila - 1 + '-' + columna;
                let coord2 = fila - 2 + '-' + columna;
                let coord3 = fila - 3 + '-' + columna;

                if (document.getElementById(`${coord}`).className === tipoFicha &&
                    document.getElementById(`${coord1}`).className === tipoFicha &&
                    document.getElementById(`${coord2}`).className === tipoFicha &&
                    document.getElementById(`${coord3}`).className === tipoFicha) {
                    ganador = true;
                }
            }

            //Fila abajo
            if ((fila + 3) <= MAXfilas) {
                let coord1 = fila + 1 + '-' + columna;
                let coord2 = fila + 2 + '-' + columna;
                let coord3 = fila + 3 + '-' + columna;

                if (document.getElementById(`${coord}`).className === tipoFicha &&
                    document.getElementById(`${coord1}`).className === tipoFicha &&
                    document.getElementById(`${coord2}`).className === tipoFicha &&
                    document.getElementById(`${coord3}`).className === tipoFicha) {
                    ganador = true;
                }
            }

            //Columna izquierda
            if ((columna - 3) >= 0) {
                let coord1 = fila + '-' + (columna - 1);
                let coord2 = fila + '-' + (columna - 2);
                let coord3 = fila + '-' + (columna - 3);

                if (document.getElementById(`${coord}`).className === tipoFicha &&
                    document.getElementById(`${coord1}`).className === tipoFicha &&
                    document.getElementById(`${coord2}`).className === tipoFicha &&
                    document.getElementById(`${coord3}`).className === tipoFicha) {
                    ganador = true;
                }
            }


            //Columna derecha
            if ((columna + 3) <= MAXcolumnas) {
                let coord1 = fila + '-' + (columna + 1);
                let coord2 = fila + '-' + (columna + 2);
                let coord3 = fila + '-' + (columna + 3);

                if (document.getElementById(`${coord}`).className === tipoFicha &&
                    document.getElementById(`${coord1}`).className === tipoFicha &&
                    document.getElementById(`${coord2}`).className === tipoFicha &&
                    document.getElementById(`${coord3}`).className === tipoFicha) {
                    ganador = true;
                }
            }

            //Diagonal izq-arriba
            if ((fila - 3) >= 0 && (columna - 3) >= 0) {
                let coord1 = (fila - 1) + '-' + (columna - 1);
                let coord2 = (fila - 2) + '-' + (columna - 2);
                let coord3 = (fila - 3) + '-' + (columna - 3);

                if (document.getElementById(`${coord}`).className === tipoFicha &&
                    document.getElementById(`${coord1}`).className === tipoFicha &&
                    document.getElementById(`${coord2}`).className === tipoFicha &&
                    document.getElementById(`${coord3}`).className === tipoFicha) {
                    ganador = true;
                }
            }

            //Diagonal izq-abajo
            if ((fila + 3) <= MAXfilas && (columna - 3) >= 0) {
                let coord1 = (fila + 1) + '-' + (columna - 1);
                let coord2 = (fila + 2) + '-' + (columna - 2);
                let coord3 = (fila + 3) + '-' + (columna - 3);

                if (document.getElementById(`${coord}`).className === tipoFicha &&
                    document.getElementById(`${coord1}`).className === tipoFicha &&
                    document.getElementById(`${coord2}`).className === tipoFicha &&
                    document.getElementById(`${coord3}`).className === tipoFicha) {
                    ganador = true;
                }
            }

            //Diagonal dch-arriba
            if ((fila - 3) >= 0 && (columna + 3) <= MAXcolumnas) {
                let coord1 = (fila - 1) + '-' + (columna + 1);
                let coord2 = (fila - 2) + '-' + (columna + 2);
                let coord3 = (fila - 3) + '-' + (columna + 3);

                if (document.getElementById(`${coord}`).className === tipoFicha &&
                    document.getElementById(`${coord1}`).className === tipoFicha &&
                    document.getElementById(`${coord2}`).className === tipoFicha &&
                    document.getElementById(`${coord3}`).className === tipoFicha) {
                    ganador = true;
                }
            }

            //Diagonal dch-abajo
            if ((fila + 3) <= MAXfilas && (columna + 3) <= MAXcolumnas) {
                let coord1 = (fila + 1) + '-' + (columna + 1);
                let coord2 = (fila + 2) + '-' + (columna + 2);
                let coord3 = (fila + 3) + '-' + (columna + 3);

                if (document.getElementById(`${coord}`).className === tipoFicha &&
                    document.getElementById(`${coord1}`).className === tipoFicha &&
                    document.getElementById(`${coord2}`).className === tipoFicha &&
                    document.getElementById(`${coord3}`).className === tipoFicha) {
                    ganador = true;
                }
            }
        }
    }
    //Diagonal dch-izq

    return ganador;


}