//Nos creamos el tablero y a los jugadores
const jugador1 = new Jugador('jugador-Rojo', 'casillaRojaMarcada', 'casillaRojaSeleccionada');
const jugador2 = new Jugador('jugador-Ama', 'casillaAmarillaMarcada', 'casillaAmarillaSeleccionada');
let jugadorActual;

const matrizTablero = new TableroMatriz(6, 8);

//--------------------------------------------
//Cargamos todos los elementos del DOM
window.addEventListener('load', function() {
    //Establecemos al rojo como primer Jugador
    jugadorActual = jugador1;

    // A partir de la matriz nos creamos la tabla en nuestro 'index.html'
    generarTablerodesdeMatriz();

    //Bindeamos la tabla/tablero
    let tablero = document.querySelector('#tablero-juego');

    //-------------------------------------------------------
    //Creamos evento 'mouseover' y seleccionamos la columna
    tablero.addEventListener('mouseover', function(e) {
        if (e.target.tagName === 'TD') {
            //Obtenemos la columna y el número total de filas
            let columna = e.target.id.charAt(2);
            let numFilas = matrizTablero.filas - 1;

            //Obtenemos las coordenadas y pintamos todas las casillas 
            for (let i = numFilas; i >= 0; i--) {
                let coord = i + '-' + columna;
                if (document.getElementById(`${coord}`).className !== jugadorActual.selectorColumna && !caillaSeleccionada(coord)) {
                    document.getElementById(`${coord}`).classList.remove('casilla');
                    document.getElementById(`${coord}`).classList.add(jugadorActual.selectorColumna);
                }
            }
        }
    });

    //-------------------------------------------------------
    //Creamos evento 'mouseout' y seleccionamos la columna
    tablero.addEventListener('mouseout', function(e) {
        if (e.target.tagName === 'TD') {
            //Obtenemos la columna y el número total de filas
            let columna = e.target.id.charAt(2);
            let numFilas = matrizTablero.filas - 1;

            //Obtenemos las coordenadas y pintamos todas las casillas 
            for (let i = numFilas; i >= 0; i--) {
                let coord = i + '-' + columna;
                if ((document.getElementById(`${coord}`).className === jugador1.selectorColumna ||
                        document.getElementById(`${coord}`).className === jugador2.selectorColumna) && !caillaSeleccionada(coord)) {
                    document.getElementById(`${coord}`).classList.remove(jugador1.selectorColumna);
                    document.getElementById(`${coord}`).classList.remove(jugador2.selectorColumna);
                    document.getElementById(`${coord}`).classList.add('casilla');
                }
            }

        }
    });

    //-------------------------------------------------------------------------
    //Creamos evento 'click' y pintamos la columna seleccionada y la última fila
    tablero.addEventListener('click', function(e) {
        if (e.target.tagName === 'TD') {
            let columna = e.target.id.charAt(2);
            let numFilas = matrizTablero.filas - 1;

            for (let i = numFilas; i >= 0; i--) {
                let coord = i + '-' + columna;

                if (document.getElementById(`${coord}`).className === jugadorActual.selectorColumna) {
                    document.getElementById(`${coord}`).classList.remove(jugadorActual.selectorColumna);
                    document.getElementById(`${coord}`).classList.add(jugadorActual.colorFicha);
                    i = 0;
                }
            }
            //Establecemos/cambiamos el turno
            comprobarSiContinuaElJuego();
            turnoJugador(columna);

        }
    });

});



//MÉTODOS
//Generamos la tabla en el html
function generarTablerodesdeMatriz() {
    let tablero = document.createElement('table');
    let tableBody = document.createElement('tbody');
    let idFila = 0;

    matrizTablero.tablero.forEach(function(datosFila) {
        let fila = document.createElement('tr');
        fila.id = idFila++;

        datosFila.forEach(function(datosColum) {
            let columnaCasilla = document.createElement('td');
            //añadimos la clase 'casilla'
            columnaCasilla.classList.add('casilla');
            columnaCasilla.id = datosColum;
            //columnaCasilla.appendChild(document.createTextNode(datosColum));
            fila.appendChild(columnaCasilla);
        });

        tableBody.appendChild(fila);
    });
    tablero.appendChild(tableBody);

    let classesTablero = ['bg-primary', 'table'];
    tablero.classList.add(...classesTablero);
    tablero.id = `tablero-juego`;

    //añadimos los estilos de bootstrap y demás
    document.querySelector("#contenedor-principal").appendChild(tablero);
}


//Nos muestra un boolean por si la casilla esta seleccionada o no
function caillaSeleccionada(coord) {
    return document.getElementById(`${coord}`).className === jugador1.colorFicha || document.getElementById(`${coord}`).className === jugador2.colorFicha;
}

//PARA EL JUEGO
//Comprobamos si hay huecos libres para continuar juego o si existe ganador
function comprobarSiContinuaElJuego() {
    const MAXfilas = matrizTablero.filas - 1;
    const MAXcolumnas = matrizTablero.columnas - 1;
    var continuaJuego = false;

    //Recorremos la tabla en orden
    for (let i = 0; i <= MAXfilas && !continuaJuego; i++) {
        for (let j = 0; j <= MAXcolumnas && !continuaJuego; j++) {
            let coord = i + '-' + j;

            //Comprobamos si hay casillas libres
            if (document.getElementById(`${coord}`).className === 'casilla' || document.getElementById(`${coord}`).className === jugadorActual.selectorColumna) {
                continuaJuego = true;
            }
            //console.log(continuaJuego);
        }
    }
    if (continuaJuego) {
        if (comprobarSiExisteGanador(jugadorActual.colorFicha)) {
            alert('Fin del juego');
        }
    } else {
        alert('Fin del juego');
    }
}

//Comprobación si hay ganador
function comprobarSiExisteGanador(tipoFicha) {
    const MAXfilas = matrizTablero.filas - 1;
    const MAXcolumnas = matrizTablero.columnas - 1;
    var ganador = false;

    //Recorremos la tabal en orden:

    //Normal
    for (let fila = 0; fila <= MAXfilas && !ganador; fila++) {
        for (let columna = 0; columna <= MAXcolumnas && !ganador; columna++) {
            let coord = fila + '-' + columna;

            //Fila arriba
            if ((fila - 3) >= 0) {
                let coord1 = fila - 1 + '-' + columna;
                let coord2 = fila - 2 + '-' + columna;
                let coord3 = fila - 3 + '-' + columna;

                if (document.getElementById(`${coord}`).className === tipoFicha &&
                    document.getElementById(`${coord1}`).className === tipoFicha &&
                    document.getElementById(`${coord2}`).className === tipoFicha &&
                    document.getElementById(`${coord3}`).className === tipoFicha) {
                    ganador = true;
                }
            }

            //Fila abajo
            if ((fila + 3) <= MAXfilas) {
                let coord1 = fila + 1 + '-' + columna;
                let coord2 = fila + 2 + '-' + columna;
                let coord3 = fila + 3 + '-' + columna;

                if (document.getElementById(`${coord}`).className === tipoFicha &&
                    document.getElementById(`${coord1}`).className === tipoFicha &&
                    document.getElementById(`${coord2}`).className === tipoFicha &&
                    document.getElementById(`${coord3}`).className === tipoFicha) {
                    ganador = true;
                }
            }

            //Columna izquierda
            if ((columna - 3) >= 0) {
                let coord1 = fila + '-' + (columna - 1);
                let coord2 = fila + '-' + (columna - 2);
                let coord3 = fila + '-' + (columna - 3);

                if (document.getElementById(`${coord}`).className === tipoFicha &&
                    document.getElementById(`${coord1}`).className === tipoFicha &&
                    document.getElementById(`${coord2}`).className === tipoFicha &&
                    document.getElementById(`${coord3}`).className === tipoFicha) {
                    ganador = true;
                }
            }


            //Columna derecha
            if ((columna + 3) <= MAXcolumnas) {
                let coord1 = fila + '-' + (columna + 1);
                let coord2 = fila + '-' + (columna + 2);
                let coord3 = fila + '-' + (columna + 3);

                if (document.getElementById(`${coord}`).className === tipoFicha &&
                    document.getElementById(`${coord1}`).className === tipoFicha &&
                    document.getElementById(`${coord2}`).className === tipoFicha &&
                    document.getElementById(`${coord3}`).className === tipoFicha) {
                    ganador = true;
                }
            }

            //Diagonal izq-arriba
            if ((fila - 3) >= 0 && (columna - 3) >= 0) {
                let coord1 = (fila - 1) + '-' + (columna - 1);
                let coord2 = (fila - 2) + '-' + (columna - 2);
                let coord3 = (fila - 3) + '-' + (columna - 3);

                if (document.getElementById(`${coord}`).className === tipoFicha &&
                    document.getElementById(`${coord1}`).className === tipoFicha &&
                    document.getElementById(`${coord2}`).className === tipoFicha &&
                    document.getElementById(`${coord3}`).className === tipoFicha) {
                    ganador = true;
                }
            }

            //Diagonal izq-abajo
            if ((fila + 3) <= MAXfilas && (columna - 3) >= 0) {
                let coord1 = (fila + 1) + '-' + (columna - 1);
                let coord2 = (fila + 2) + '-' + (columna - 2);
                let coord3 = (fila + 3) + '-' + (columna - 3);

                if (document.getElementById(`${coord}`).className === tipoFicha &&
                    document.getElementById(`${coord1}`).className === tipoFicha &&
                    document.getElementById(`${coord2}`).className === tipoFicha &&
                    document.getElementById(`${coord3}`).className === tipoFicha) {
                    ganador = true;
                }
            }

            //Diagonal dch-arriba
            if ((fila - 3) >= 0 && (columna + 3) <= MAXcolumnas) {
                let coord1 = (fila - 1) + '-' + (columna + 1);
                let coord2 = (fila - 2) + '-' + (columna + 2);
                let coord3 = (fila - 3) + '-' + (columna + 3);

                if (document.getElementById(`${coord}`).className === tipoFicha &&
                    document.getElementById(`${coord1}`).className === tipoFicha &&
                    document.getElementById(`${coord2}`).className === tipoFicha &&
                    document.getElementById(`${coord3}`).className === tipoFicha) {
                    ganador = true;
                }
            }

            //Diagonal dch-abajo
            if ((fila + 3) <= MAXfilas && (columna + 3) <= MAXcolumnas) {
                let coord1 = (fila + 1) + '-' + (columna + 1);
                let coord2 = (fila + 2) + '-' + (columna + 2);
                let coord3 = (fila + 3) + '-' + (columna + 3);

                if (document.getElementById(`${coord}`).className === tipoFicha &&
                    document.getElementById(`${coord1}`).className === tipoFicha &&
                    document.getElementById(`${coord2}`).className === tipoFicha &&
                    document.getElementById(`${coord3}`).className === tipoFicha) {
                    ganador = true;
                }
            }
        }
    }
    return ganador;
}

//Cambio del turno de jugador
function turnoJugador(columna) {
    //Guardamos el valor anterior
    let seleccionAnterior = jugadorActual.selectorColumna;

    if (jugadorActual === jugador1) {
        jugadorActual = jugador2

    } else {
        jugadorActual = jugador1
    }

    for (let i = matrizTablero.filas - 1; i >= 0; i--) {
        let coord = i + '-' + columna;
        if (!caillaSeleccionada(coord)) {
            document.getElementById(`${coord}`).classList.remove(seleccionAnterior);
            document.getElementById(`${coord}`).classList.add(jugadorActual.selectorColumna);
        }
    }
}