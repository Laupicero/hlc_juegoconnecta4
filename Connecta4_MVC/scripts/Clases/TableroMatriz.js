class TableroMatriz {
    constructor(filas, columnas) {
        this.filas = filas;
        this.columnas = columnas;
        this.tablero = this.crearMatriz(filas, columnas);
    }

    //Métodos
    //Nos creamos la matriz
    crearMatriz(filas, columnas) {

        let matriz = new Array(filas);

        for (let i = 0; i < matriz.length; i++) {
            matriz[i] = new Array(columnas);
        }

        this.rellenarMatrizVacia(matriz);
        return matriz;
    }

    //Rellenamos la Matriz
    rellenarMatrizVacia(matriz) {
        for (let i = 0; i < matriz.length; i++) {
            for (let j = 0; j < matriz[i].length; j++) {
                matriz[i][j] = i + `-` + j;
            }
        }
    }


}